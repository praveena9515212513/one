﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    public GameObject destroydVersion;
    public void Destroy()
    {
        Instantiate(destroydVersion, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}